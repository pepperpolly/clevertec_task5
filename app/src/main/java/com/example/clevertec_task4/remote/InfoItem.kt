package com.example.clevertec_task4.remote

data class InfoItem(
    val gps_x: String,
    val gps_y: String,
    val GPS_X: String,
    val GPS_Y: String
)