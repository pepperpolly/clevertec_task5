package com.example.clevertec_task4.remote


import io.reactivex.Observable
import retrofit2.http.GET

interface BankApi {
    @GET("atm?city=Гомель")
    fun getATMInfo(): Observable<List<InfoItem>>

    @GET("infobox?city=Гомель")
    fun getInfoboxInfo(): Observable<List<InfoItem>>

    @GET("filials_info?city=Гомель")
    fun getFilialsInfo(): Observable<List<InfoItem>>
}