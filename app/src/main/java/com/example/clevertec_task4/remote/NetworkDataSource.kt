package com.example.clevertec_task4.remote


import io.reactivex.Observable
import javax.inject.Inject

class NetworkDataSource @Inject constructor(private val bankApi: BankApi) {

    fun getATMInfo(): Observable<List<InfoItem>> =
        bankApi.getATMInfo()

    fun getInfoboxInfo(): Observable<List<InfoItem>> = bankApi.getInfoboxInfo()

    fun getFilialsInfo(): Observable<List<InfoItem>> = bankApi.getFilialsInfo()
}