package com.example.clevertec_task4.remote

data class CoordinatesItem(
    val gps_x: String,
    val gps_y: String
)