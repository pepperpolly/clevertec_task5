package com.example.clevertec_task4

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.clevertec_task4.remote.CoordinatesItem
import com.example.clevertec_task4.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt
private const val LAT = 52.425163
private const val LON = 31.015039

@HiltViewModel
class MapViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    val coordinates: MutableLiveData<MutableList<CoordinatesItem>> =
        MutableLiveData(mutableListOf())
    private var tempList = mutableListOf<CoordinatesItem>()

    @SuppressLint("CheckResult")
    fun getCoordinates() {
        repository.getATMInfo().observeOn(AndroidSchedulers.mainThread())
            .mergeWith(repository.getFilialsInfo())
            .mergeWith(repository.getInfoboxInfo().doOnComplete {
                tempList.sortBy {
                    sqrt(
                        (max(LAT, it.gps_x.toDouble()) - min(
                            LAT, it.gps_x.toDouble()
                        )).pow(2.0) + (max(
                            LON,
                            it.gps_y.toDouble()
                        ) - min(
                            LON,
                            it.gps_y.toDouble()
                        ))
                            .pow(2.0)
                    )
                }
                coordinates.postValue(tempList.subList(0, 10))
            })
            .subscribeOn(Schedulers.io())
            .subscribe({ result ->

                if (result[0].gps_x == null) tempList.addAll(result.map {
                    CoordinatesItem(
                        it.GPS_X,
                        it.GPS_Y
                    )
                })
                else tempList.addAll(result.map { CoordinatesItem(it.gps_x, it.gps_y) })
            }, { error ->
                error.printStackTrace()
            })
    }
}