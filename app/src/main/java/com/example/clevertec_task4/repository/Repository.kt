package com.example.clevertec_task4.repository


import com.example.clevertec_task4.remote.InfoItem
import com.example.clevertec_task4.remote.NetworkDataSource
import io.reactivex.Observable
import javax.inject.Inject

class Repository @Inject constructor(private val networkDataSource: NetworkDataSource) {
    fun getATMInfo(): Observable<List<InfoItem>> =
        networkDataSource.getATMInfo()

    fun getInfoboxInfo(): Observable<List<InfoItem>> =
        networkDataSource.getInfoboxInfo()

    fun getFilialsInfo(): Observable<List<InfoItem>> =
        networkDataSource.getFilialsInfo()
}