package com.example.clevertec_task4

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.clevertec_task4.databinding.ActivityMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

private const val GOMEL_LAT = 52.4345000
private const val GOMEL_LON = 30.9754000
private const val ZOOM_LEVEL = 10f
private val GOMEL = LatLng(GOMEL_LAT, GOMEL_LON)

@AndroidEntryPoint
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var map: GoogleMap? = null
    private lateinit var binding: ActivityMapsBinding
    private val viewModel by viewModels<MapViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        viewModel.coordinates.observe(this) { list ->
            list?.forEach {
                    map?.addMarker(
                        MarkerOptions().position(
                            LatLng(
                                it.gps_x.toDouble(),
                                it.gps_y.toDouble()
                            )
                        ).title(getString(R.string.atm))
                    )
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        viewModel.getCoordinates()
        map = googleMap
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(GOMEL, ZOOM_LEVEL))
    }
}