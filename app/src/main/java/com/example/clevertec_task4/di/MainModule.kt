package com.example.clevertec_task4.di

import com.example.clevertec_task4.remote.BankApi
import com.example.clevertec_task4.remote.NetworkDataSource
import com.example.clevertec_task4.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MainModule {
    @Provides
    @Singleton
    fun provideRepo(
        networkDataSource: NetworkDataSource
    ): Repository {
        return Repository(
            networkDataSource
        )
    }

    @Provides
    @Singleton
    fun provideWeatherApi(): BankApi {
        return Retrofit.Builder()
            .baseUrl("https://belarusbank.by/api/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(BankApi::class.java)
    }
}